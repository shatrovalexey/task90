<?php
	/**
	* Создать страницу с авторизацией пользователя: логин и пароль и реализовать в ней:
	* возможность регистрации пользователя (email, логин, пароль, ФИО),
	* при входе в "личный кабинет" возможность сменить пароль и ФИО.
	* использовать "чистый" PHP 5.6 и выше (без фреймворков) и MySQL 5.5 и выше, дизайн не важен, верстка тоже простая. Наворотов не нужно, хотим
	* посмотреть просто Ваш код.
	*/

	/**
	* Валидация значения массива по ключу
	*
	* @param string $key - ключ
	* @param array $request - массив
	* @throws \Exception
	*
	* @return boolean - результат
	*/
	function validate( string $key , array &$request ) : bool {
		switch ( $key ) { // e-mail
			case 'email' : {
				if ( ! preg_match( '{^\w[\w\.\-]+\w\@\w[\w\.\-]+\w$}s' , $request[ $key ] ) ) {
					throw new \Exception( 'Формат e-mail не верен' ) ;
				}

				return true ;
			}
			case 'fio' : { // ФИО
				if ( ! preg_match( '{^\S+ \S+ \S+$}s' , $request[ $key ] ) ) {
					throw new \Exception( 'Формат ФИО не верен' ) ;
				}

				return true ;
			}
			case 'password' : { // пароль
				if ( $request[ $key ] != $request[ $key . '2' ] ) {
					throw new \Exception( 'Пароли не совпадают' ) ;
				}
				if ( ! preg_match( '{^\S{8,}$}s' , $request[ $key ] ) ) {
					throw new \Exception( 'Формат пароля не верен' ) ;
				}

				return true ;
			}
			case 'is_logged_in' : { // авторизован ли пользователь
				if ( empty( $request[ $key ] ) ) {
					throw new \Exception( 'пользователь не найден' ) ;
				}

				return true ;
			}
		}

		return false ;
	}

	/**
	* Обработка HTTP-запроса
	*
	* @param \PDO $dbh - объект подключения к СУБД
	* @param array $request - HTTP-аргументы
	* @param array $server - переменные окружения сервера
	*/
	function serve( \PDO $dbh , array &$request , array &$server ) {
		session_start( ) ;

		$user = null ;
		$result = [ ] ;
		$session_id = session_id( ) ;

		try {
			/**
			* Что-то вроде миграций
			*/
			foreach ( [
				"
SET NAMES utf8mb4 ;
				" , "
CREATE TABLE IF NOT EXISTS `user`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`fio` VARCHAR( 100 ) NOT null COMMENT 'ФИО' ,
	`password` CHAR( 40 ) NOT null COMMENT 'хэш пароля' ,
	`session_id` CHAR( 40 ) null COMMENT 'идентификатор сессии' ,
	`email` VARCHAR( 100 ) NOT null COMMENT 'e-mail' ,

	`created_at` DATETIME NOT null COMMENT 'дата-время создания' ,
	`updated_at` DATETIME NOT null COMMENT 'дата-время изменения' ,

	PRIMARY KEY( `id` ) ,
	UNIQUE( `session_id` ) ,
	UNIQUE( `email` )
) COMMENT = 'пользователь' ;
				" , "
CREATE TABLE IF NOT EXISTS `session`(
	`id` CHAR( 40 ) NOT null COMMENT 'идентификатор' ,
	`created_at` DATETIME NOT null COMMENT 'дата-время создания' ,
	`updated_at` DATETIME NOT null COMMENT 'дата-время изменения' ,

	PRIMARY KEY( `id` ) ,
	INDEX( `updated_at` )
) COMMENT = 'сессия' ;
				" ,
			] as $sql ) {
				$dbh->exec( $sql ) ;
			}

			$dbh->exec( " -- удалить устаревшие сессии
DELETE
	`s1`.*
FROM
	`session` AS `s1`
WHERE
	( `s1`.`updated_at` < adddate( current_timestamp( ) , INTERVAL -1 DAY ) ) ;
			" ) ;
			$sth_ins = $dbh->prepare( " -- создать сессию
INSERT INTO
	`session`
SET
	`created_at` := current_timestamp( ) ,
	`updated_at` := current_timestamp( ) ,
	`id` := :session_id
ON DUPLICATE KEY UPDATE
	`updated_at` := current_timestamp( ) ;
			" ) ;
			$sth_ins->execute( [
				':session_id' => $session_id ,
			] ) ;
			$sth_ins->closeCursor( ) ;

			$sth_sel = $dbh->prepare( " -- получить данные о текущем пользователе
SELECT SQL_SMALL_RESULT
	`u1`.* ,
	`u1`.`id` IS NOT null AS `is_logged_in` ,
	`s1`.`id` AS `session_id` ,
	`s1`.`created_at` AS `session_created_at` ,
	`s1`.`updated_at` AS `session_updated_at`
FROM
	`session` AS `s1`

	LEFT OUTER JOIN `user` AS `u1` ON
	( `u1`.`session_id` = `s1`.`id` )
WHERE
	( `s1`.`id` = :session_id )
LIMIT 1 ;
			" ) ;
			$sth_sel->execute( [
				':session_id' => $session_id ,
			] ) ;
			$user = $sth_sel->fetch( \PDO::FETCH_ASSOC ) ;
			$sth_sel->closeCursor( ) ;

			/**
			* Что-то вроде контроллера
			*/
			$result = ( function( $dbh , &$request , &$server , &$session_id , &$user ) {
				switch ( @$request[ 'action' ] ) {
					case '' :
					case 'index' : { // главная страница
						return [ 'view' => 'index' , ] ;
					}
					case 'register' : { // форма регистрации
						return [ 'view' => 'register' , ] ;
					}
					case 'login' : { // форма авторизации
						return [ 'view' => 'login' , ] ;
					}
					case 'lk' : { // личный кабинет
						validate( 'is_logged_in' , $user ) ;

						return [ 'view' => 'lk' , ] ;
					}
					case 'do-register' : { // процесс регистрации
						validate( 'email' , $request ) ;
						validate( 'fio' , $request ) ;
						validate( 'password' , $request ) ;

						$sth_sel = $dbh->prepare( "
SELECT SQL_SMALL_RESULT
	`u1`.`id` AS `user_id`
FROM
	`user` AS `u1`
WHERE
	( `u1`.`email` = :email )
LIMIT 1 ;
						" ) ;
						$sth_sel->execute( [
							':email' => $request[ 'email' ] ,
						] ) ;
						if ( list ( $user_id ) = $sth_sel->fetch( \PDO::FETCH_NUM ) ) {
							throw new \Exception( 'пользователь с таким e-mail существует' ) ;
						}

						$sth_ins = $dbh->prepare( "
INSERT IGNORE INTO
	`user`
SET
	`session_id` := :session_id ,
	`fio` := :fio ,
	`email` := :email ,
	`password` := sha1( :password ) ,
	`created_at` := current_timestamp( ) ,
	`updated_at` := current_timestamp( ) ;
						" ) ;
						$sth_ins->execute( [
							':session_id' => $session_id ,
							':password' => $request[ 'password' ] ,
							':fio' => $request[ 'fio' ] ,
							':email' => $request[ 'email' ] ,
						] ) ;
						$user[ 'id' ] = $dbh->lastInsertId( ) ;
						$sth_ins->closeCursor( ) ;

						if ( $user[ 'id' ] <= 0 ) {
							throw new \Exception( 'внутренняя ошибка сервера' ) ;
						}

						$user[ 'session_id' ] = $session_id ;

						foreach ( [ 'fio' , 'email' , ] as $key ) {
							$user[ $key ] = &$request[ $key ] ;
						}

						throw new \Exception( 'успех' ) ;
					}
					case 'do-login' : { // процесс авторизации
						$sth_sel = $dbh->prepare( "
SELECT SQL_SMALL_RESULT
	`u1`.`id` AS `user_id`
FROM
	`user` AS `u1`
WHERE
	( `u1`.`email` = :email ) AND
	( `u1`.`password` = sha1( :password ) )
LIMIT 1 ;
						" ) ;
						$sth_sel->execute( [
							':email' => @$request[ 'email' ] ,
							':password' => @$request[ 'password' ] ,
						] ) ;

						list ( $user_id ) = $sth_sel->fetch( \PDO::FETCH_NUM ) ;

						if ( empty( $user_id ) ) {
							throw new \Exception( 'пользователь не найден' ) ;
						}

						$sth_upd = $dbh->prepare( "
UPDATE
	`user` AS `u1`
SET
	`u1`.`session_id` := :session_id ,
	`u1`.`updated_at` := current_timestamp( )
WHERE
	( `u1`.`id` = :user_id ) ;
						" ) ;
						$sth_upd->execute( [
							':user_id' => $user_id ,
							':session_id' => $session_id ,
						] ) ;

						if ( ! $sth_upd->rowCount( ) ) {
							throw new \Exception( 'внутренняя ошибка' ) ;
						}

						return [ 'redirect' => 'index' , ] ;
					}
					case 'do-logout' : { // процесс разавторизации
						validate( 'is_logged_in' , $user ) ;

						$sth_upd = $dbh->prepare( "
UPDATE
	`user` AS `u1`

	INNER JOIN `session` AS `s1` ON
	( `u1`.`session_id` = `s1`.`id` )
SET
	`u1`.`session_id` := null
WHERE
	( `s1`.`id` = :session_id ) ;
						" ) ;
						$sth_upd->execute( [
							':session_id' => $session_id ,
						] ) ;

						if ( ! $sth_upd->rowCount( ) ) {
							throw new \Exception( 'пользователь не найден' ) ;
						}

						return [ 'redirect' => 'index' , ] ;
					}
					case 'set-password' : { // процесс изменения пароля
						validate( 'is_logged_in' , $user ) ;
						validate( 'password' , $request ) ;

						$sth_upd = $dbh->prepare( "
UPDATE
	`user` AS `u1`

	INNER JOIN `session` AS `s1` ON
	( `u1`.`session_id` = `s1`.`id` )
SET
	`u1`.`password` := sha1( :password )
WHERE
	( `s1`.`id` = :session_id ) ;
						" ) ;
						$sth_upd->execute( [
							':password' => $request[ 'password' ] ,
							':session_id' => $session_id ,
						] ) ;

						if ( ! $sth_upd->rowCount ) {
							throw new \Exception( 'пользователь не найден' ) ;
						}

						throw new \Exception( 'успех' ) ;
					}
					case 'set-fio' : { // процесс изменения ФИО
						validate( 'is_logged_in' , $user ) ;
						validate( 'fio' , $request ) ;

						$sth_upd = $dbh->prepare( "
UPDATE
	`user` AS `u1`

	INNER JOIN `session` AS `s1` ON
	( `u1`.`session_id` = `s1`.`id` )
SET
	`u1`.`fio` := :fio
WHERE
	( `s1`.`id` = :session_id ) ;
						" ) ;
						$sth_upd->execute( [
							':fio' => $request[ 'fio' ] ,
							':session_id' => $session_id ,
						] ) ;

						if ( ! $sth_upd->rowCount( ) ) {
							throw new \Exception( 'ФИО не менялись' ) ;
						}

						return [ 'redirect' => 'lk' , ] ;
					}
					default : { // страница ошибки 404
						return [ 'status' => '404 Not found' , ] ;
					}
				}
			} )( $dbh , $request , $server , $session_id , $user ) ;

			foreach ( [
				'request' => &$request ,
				'server' => &$server ,
				'session' => &$session ,
			] as $key => $value ) {
				if ( isset( $result[ 'data' ][ $key ] ) ) {
					continue ;
				}

				$result[ 'data' ][ $key ] = $value ;
			}
		} catch ( \Exception $exception ) {
			$result = [
				'view' => 'message' ,
				'data' => [
					'message' => $exception->getMessage( ) ,
				]
			] ;
		}

		// вероятный редирект
		if ( ! empty( $result[ 'redirect' ] ) ) {
			header( 'Location: ?action=' . urlencode( $result[ 'redirect' ] ) ) ;

			exit( 0 ) ;
		}

		// вывод шаблона страницы
		return ( function( $view , &$data , &$user , $status ) {
			$view_template = '../view/%s.php' ;
			$view_required = sprintf( $view_template , $view ) ;
			$view_index = sprintf( $view_template , 'main' ) ;

			if ( $status ) {
				header( 'HTTP/1.0 ' . $status ) ;
			}

			header( 'Content-Type: text/html; charset=utf-8' ) ;

			$values = [
				'data' => &$data ,
				'user' => &$user ,
				'view_required' => &$view_required ,
			] ;

			extract( $values ) ;

			require( $view_index ) ;
		} )( $result[ 'view' ] , $result[ 'data' ] , $user , @$result[ 'status' ] ) ;
	}

	// подключение к СУБД
	/**
	* @var \PDO $dbh - объект подключения к СУБД
	*/
	$dbh = new \PDO( 'mysql:host=localhost;dbname=task90;charset=utf8mb4' , 'root' , 'f2ox9erm' ) ;

	serve( $dbh , $_REQUEST , $_SERVER ) ;