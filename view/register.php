<form method="post">
	<fieldset>
		<legend>регистрация</legend>

		<input type="hidden" name="action" value="do-register">
		<p><label>
			<span>ФИО:</span>
			<input name="fio" pattern="^[a-zA-Zа-яА-ЯёЁ]+ [a-zA-Zа-яА-ЯёЁ]+ [a-zA-Zа-яА-ЯёЁ]+$" required>
		</label>
		<p><label>
			<span>e-mail:</span>
			<input type="email" name="email" required>
		</label>
		<p><label>
			<span>пароль:</span>
			<input type="password" name="password" required pattern="\S{8,}">
		</label>
		<p><label>
			<span>подтверждение пароля:</span>
			<input type="password" name="password2" required pattern="\S{8,}">
		</label>
		<p><label>
			<span>отправить</span>
			<input type="submit" value="&rarr;">
		</label>
	</fieldset>
</form>