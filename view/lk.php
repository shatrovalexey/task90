<form type="post">
	<input type="hidden" name="action" value="set-fio">
	<fieldset>
		<legend>изменение ФИО</legend>
		<p><label>
			<span>ФИО:</span>
			<input name="fio" pattern="^[a-zA-Zа-яА-ЯёЁ]+ [a-zA-Zа-яА-ЯёЁ]+ [a-zA-Zа-яА-ЯёЁ]+$" required value="<?=htmlspecialchars( $user[ 'fio' ] )?>">
		</label>
		<p><label>
			<span>отправить</span>
			<input type="submit" value="&rarr;">
		</label>
	</fieldset>
</form>
<form type="post">
	<input type="hidden" name="action" value="set-password">
	<fieldset>
		<legend>изменение пароля</legend>
		<p><label>
			<span>пароль:</span>
			<input type="password" name="password" required pattern="\S{8,}">
		</label>
		<p><label>
			<span>подтверждение пароля:</span>
			<input type="password" name="password2" required pattern="\S{8,}">
		</label>
		<p><label>
			<span>отправить</span>
			<input type="submit" value="&rarr;">
		</label>
	</fieldset>
</form>