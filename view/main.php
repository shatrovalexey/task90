<!DOCTYPE html>

<html>
	<head>
		<title><?=htmlspecialchars( $title )?></title>
		<meta charset="utf-8">
	</head>
	<body>
		<menu>
			<?php if ( $user[ 'is_logged_in' ] ) { ?>
			<ul>
				<li>
					<a href="/">главная</a>
				</li>
				<li>
					<a href="/?action=lk">личный кабинет</a>
				</li>
				<li>
					<a href="/?action=do-logout">выход</a>
				</li>
			</ul>
			<?php } else { ?>
			<ul>
				<li>
					<a href="/">главная</a>
				</li>
				<li>
					<a href="/?action=register">регистрация</a>
				</li>
				<li>
					<a href="/?action=login">авторизация</a>
				</li>
			</ul>
			<?php } ?>
		</menu>
		<div>
			<?php require( $view_required ) ; ?>
		</div>
		<footer>
			<div>Автор: Шатров Алексей Сергеевич &lt;mail@ashatrov.ru&gt;</div>
		</footer>
	</body>
</html>